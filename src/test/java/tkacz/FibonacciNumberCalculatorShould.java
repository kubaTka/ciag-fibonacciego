package tkacz;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class FibonacciNumberCalculatorShould {
    private final FibonacciNumberCalculator calculator = new FibonacciNumberCalculator();

    @Test
    void returnZeroForTheFirsElement() {
        // given
        final var index = 0;

        // when
        final var result = calculator.calculateFibonacciNumber(index);

        // then
        assertThat(result).isEqualTo(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2})
    void returnOneForTheFirstTwoElements(int index) {
        // when
        final var result = calculator.calculateFibonacciNumber(index);

        // then
        assertThat(result).isEqualTo(1);
    }

    @Test
    void return19thElement() {
        // given
        final var index = 19;

        // when
        final var result = calculator.calculateFibonacciNumber(index);

        // then
        assertThat(result).isEqualTo(4181);
    }

    @Test
    void runFast() {
        // given
        final var largeIndex = 44;
        final var startTime = System.currentTimeMillis();

        // when
        final var result = calculator.calculateFibonacciNumber(largeIndex);

        // then
        final var elapsed = System.currentTimeMillis() - startTime;
        assertThat(result).isEqualTo(701408733);
        assertThat(elapsed).isLessThan(1000);
    }
}