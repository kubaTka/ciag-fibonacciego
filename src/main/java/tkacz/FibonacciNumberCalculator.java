package tkacz;

import java.util.HashMap;
import java.util.Map;

public class FibonacciNumberCalculator {
    public int calculateFibonacciNumber(int index) {
        final var cache = new HashMap<Integer, Integer>();
        return calculateFibonacciNumber(index, cache);
    }

    private int calculateFibonacciNumber(int index, Map<Integer, Integer> cache) {
        if (index == 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else if (index == 2) {
            return 1;
        } else if (cache.containsKey(index)) {
            return cache.get(index);
        }
        final var result = calculateFibonacciNumber(index - 1, cache) + calculateFibonacciNumber(index - 2, cache);
        cache.put(index, result);
        return result;
    }
}
